﻿using AsynchronousWeatherDataFetcher;
using Newtonsoft.Json;

// create new instance of WeatherData
var weatherData = new WeatherData();

// get data
await weatherData.FetchDataAsync("Thanh pho Ho Chi Minh");

// using SerializeObject to convert Obj to JsonString
string jsonObj = JsonConvert.SerializeObject(weatherData);

// create file name
// replace ** to where you want to store .json
string filePath = $"**\\{weatherData.Location}.json";

// using File.WriteAllText to write .json file
await File.WriteAllTextAsync(filePath, jsonObj);


