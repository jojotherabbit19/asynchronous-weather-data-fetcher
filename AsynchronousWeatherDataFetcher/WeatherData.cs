﻿using Newtonsoft.Json;

namespace AsynchronousWeatherDataFetcher
{
    public class WeatherData
    {
        // your API Key here: 
        private readonly string APIKey = "d527a50cf4cd03ebdcad7ec02b5eb507";
        public string? Location { get; set; }
        public double Temperature { get; set; }
        public int Humidity { get; set; }
        public double WindSpeed { get; set; }
        public string? Description { get; set; }

        public async Task FetchDataAsync(string location)
        {
            // create new instance of HttpClient
            var client = new HttpClient();

            // get reponse
            var response = await client.GetAsync($"https://api.openweathermap.org/data/2.5/weather?q={location}&appid={APIKey}&units=metric");

            // print response code and messages
            Console.WriteLine($"Response status code: {(int)response.StatusCode} | Response messages: {response.ReasonPhrase}");

            // if statusCode is not 200 => throw Exception
            if (response.IsSuccessStatusCode)
            {
                // get content of response
                var content = await response.Content.ReadAsStringAsync();

                // using DeserializeObject convert jsonString to Obj
                // using dynamic instead var because JSON data can have varying structures,
                // which means that the type of the data may not be known until runtime
                // but dynamic will resolved at runtime(var compile time)
                dynamic? weather = JsonConvert.DeserializeObject(content);

                // assign value
                Location = weather?.name;
                Temperature = weather?.main.temp;
                Humidity = weather?.main.humidity;
                WindSpeed = weather?.wind.speed;
                Description = weather?.weather[0].description;

                // print data
                Console.WriteLine($"Weather in {Location} today:\n Temp: {Temperature}\n Humidity: {Humidity}\n Wind Speed: {WindSpeed}\n Description: {Description}");
            }
            else
            {
                Console.WriteLine(new ArgumentException("Failed to fetch weather data."));
            }
        }
    }
}
